<?php
require '../vendor/autoload.php';

// Instantiate the app
$config = require __DIR__ . '/../src/settings.php';


$app = new \Slim\App(['settings' => $config]);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

$app->run();