<?php

use Slim\Http\Request;
use Slim\Http\Response;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

$app->post('/api/register', function (Request $request, Response $response, array $args) use ($container)
{
    $userModel = new Users($this->db);
    $userData = $request->getParsedBody();

    if (!empty($userData['email']) &&
        !empty($userData['login']) &&
        !empty($userData['password']) &&
        !empty($userData['gender']) &&
        !empty($userData['name']) &&
        !empty($userData['surname']))
    {

        $necessaryUserData = true;
    } else {

        $necessaryUserData = false;
    }

    if ($necessaryUserData) {

        $userModel->register($userData);
        $status = 201;
    } else {

        $status = 400;
    }

    return $response->withStatus($status);
});

$app->post('/api/login', function(Request $request, Response $response, array $args) {
    $data = $request->getParsedBody();

    if (!empty($data['login']) && !empty($data['password'])) {
        try {
            $usersModel = new Users($this->db);
            $user = $usersModel->verify($data['login'], $data['password']);
            if ($user) {
                $signer = new Sha256();

                $token = (new Builder())
                    ->setIssuer('http://chat-app') // Configures the issuer (iss claim)
                    ->setAudience('http://chat-app') // Configures the audience (aud claim)
                    ->setId('xkosvica_chat_app_mendelu', true) // Configures the id (jti claim), replicating as a header item
                    ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
                    ->setNotBefore(time()) // Configures the time that the token can be used (nbf claim)
                    ->setExpiration(time() + 3600) // Configures the expiration time of the token (exp claim)
                    ->set('id', $user['id_users']) // Configures a new claim, called "uid"
                    ->set('login', $user['login']) // Configures a new claim, called "uid"
                    ->sign($signer, getenv('TOKEN_KEY')) // creates a signature using our key from .env
                    ->getToken(); // Retrieves the generated token

                return $response->withJson([
                    'token' => (string) $token,
                    'userId' => $user['id_users'],
                    'name' => $user['name'],
                    'surname' => $user['surname'],
                    'login' => $user['login'],
                    'email' => $user['email'],
                    'gender' => $user['gender']
                ], 201);
            } else {
                return $response->withStatus(404);
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return $response->withStatus(500);
        }
    } else {
        return $response->withStatus(400);
    }
});

$app->group('/api/auth', function() use ($app) {
    $app->get('/rooms', function (Request $request, Response $response, array $args) {
        $roomsModel = new Rooms($this->db);

        try {
            $data = $roomsModel->all();
            return $response->withJson($data);
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            return $response->withStatus(500);
        }
    });

    $app->get('/rooms/{id}', function (Request $request, Response $response, array $args) {
        if(!empty($args['id'])) {
            $rm = new Rooms($this->db);
            try {
                $info = $rm->find($args['id']);
                if($info) {
                    return $response->withJson($info);
                } else {
                    return $response->withStatus(404);
                }
            } catch (Exception $ex) {
                $this->logger->error($ex->getMessage());
                return $response->withStatus(500);
            }
        } else {
            return $response->withStatus(400);
        }
    });

    $app->post('/rooms', function (Request $request, Response $response, array $args) {
        $roomsModel = new Rooms($this->db);
        try {
            $data = $request->getParsedBody();
            if(!empty($data['title'])) {
                $token = $request->getAttribute('token');
                $userId = $token->getClaim('id');
                $roomsModel->add($data['title'], $userId);
                return $response->withStatus(201);
            } else {
                return $response->withStatus(400);
            }
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            return $response->withStatus(500);
        }
    });

    $app->get('/messages/{roomId}', function (Request $request, Response $response, array $args) {
        if(!empty($args['roomId'])) {
            $rm = new Messages($this->db);
            try {
                $messages = $rm->getAllByRoomId($args['roomId']);
                return $response->withJson($messages);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                return $response->withStatus(500);
            }
        } else {
            return $response->withStatus(400);
        }
    });

    $app->post('/messages/{roomId}', function (Request $request, Response $response, array $args) {
        $requestBody = $request->getParsedBody();

        if(!empty($args['roomId']) && !empty($requestBody['idUserFrom']) && !empty($requestBody['message'])) {
            $messagesModel = new Messages($this->db);
            try {
                $messagesModel->add($args['roomId'], $requestBody['idUserFrom'], $requestBody['idUserTo'], $requestBody['message']);
                return $response->withStatus(201);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                return $response->withStatus(500);
            }
        } else {
            return $response->withStatus(400);
        }
    });

    $app->post('/rooms/insert-user', function (Request $request, Response $response, array $args) {
        $requestBody = $request->getParsedBody();
        $userModel = new Users($this->db);

        try {
            $userModel->insertUserIntoRoom($requestBody['userId'], $requestBody['roomId']);
            return $response->withStatus(201);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return $response->withStatus(500);
        }
    });

    $app->post('/kick-user', function (Request $request, Response $response, array $args) {
        $requestBody = $request->getParsedBody();
        $roomsModel = new Rooms($this->db);

        try {
            $roomsModel->kickUserFromRoom($requestBody['roomId'], $requestBody['userId']);
            return $response->withStatus(201);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return $response->withStatus(500);
        }
    });

    $app->post('/is-user-kicked', function (Request $request, Response $response, array $args) {
        $requestBody = $request->getParsedBody();
        $roomsModel = new Rooms($this->db);

        try {
            $kicked = $roomsModel->isUserKicked($requestBody['roomId'], $requestBody['userId']);
            return $response->withJson($kicked);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return $response->withStatus(500);
        }
    });

    $app->post('/rooms/remove-user-from-room', function (Request $request, Response $response, array $args) {
        $requestBody = $request->getParsedBody();

        $userModel = new Users($this->db);
        if (!empty($requestBody['userId']) && !empty($requestBody['roomId'])) {
            try {
                $userModel->removeUserFromRoom($requestBody['userId'], $requestBody['roomId']);
                return $response->withStatus(201);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                return $response->withStatus(500);
            }
        } else {
            return $response->withStatus(400);
        }

    });

    $app->post('/lock-room/{roomId}', function (Request $request, Response $response, array $args) {
        $roomsModel = new Rooms($this->db);
        if (!empty($args['roomId'])) {
            try {
                $roomsModel->lockOrUnlock($args['roomId']);
                return $response->withStatus(201);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                return $response->withStatus(500);
            }
        } else {
            return $response->withStatus(400);
        }

    });

    $app->get('/get-users-in-room/{roomId}', function (Request $request, Response $response, array $args) {
        $roomsModel = new Rooms($this->db);
        if (!empty($args['roomId'])) {
            try {
                $users = $roomsModel->getUsersInRoom($args['roomId']);
                return $response->withJson($users);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                return $response->withStatus(500);
            }
        } else {
            return $response->withStatus(400);
        }
    });

    // Place for other secured routes like GET /messages.

})->add(function(Request $request, Response $response, $next) {
    $rawToken = $request->getHeaderLine('Authorization');
    if($rawToken) {
        $token = (new Parser())->parse((string) $rawToken);

        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer('http://chat-app');
        $data->setAudience('http://chat-app');
        $data->setId('xkosvica_chat_app_mendelu');

        $signer = new Sha256();

        define("TOKEN_KEY", "ioji883tjg8gsdo78678ghjg0ynkhi7yfgsd");

        if($token->validate($data) &&
            $token->verify($signer, TOKEN_KEY)) {
            $request = $request->withAttribute('token', $token);
            return $next($request, $response, $token);
        }
    }
    return $response->withStatus(401);  //unauthorized
});

