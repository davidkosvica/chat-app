<?php

use Dotenv\Dotenv;

$dotenv = new Dotenv('../');
$dotenv->load();

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$config['logger'] = ['name' => 'chat-app', 'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log', 'level' => \Monolog\Logger::DEBUG];

//$config['db']['host']   = getenv('HOST');
//$config['db']['user']   = getenv('DB_USER');
//$config['db']['pass']   = getenv('DB_PASS');
//$config['db']['dbname'] = getenv('DB_NAME');
//$config['db']['dbtype'] = getenv('DB_TYPE');

$config['db']['host']   = 'localhost';
$config['db']['user']   = 'root';
$config['db']['pass']   = 'leeloodallas';
$config['db']['dbname'] = 'chat-app';
$config['db']['dbtype'] = 'mysql';

return $config;