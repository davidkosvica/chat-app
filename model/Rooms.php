<?php
/**
 * Created by PhpStorm.
 * User: xpisarov
 * Date: 13.03.2019
 * Time: 9:54
 */

class Rooms extends Model
{
    function add($title, $idOwner, $lock = false) {
        $stmt = $this->db->prepare('INSERT INTO `rooms` (`created`, `title`, `id_users_owner`, `lock`) VALUES (NOW(), :t, :id, :l)');
        $stmt->bindValue(':t', $title, PDO::PARAM_STR);
        $stmt->bindValue(':id', $idOwner, PDO::PARAM_INT);
        $stmt->bindValue(':l', $lock ? "true" : "false", PDO::PARAM_STR);

        return $stmt->execute();
    }

    function all() {
        $stmt = $this->db->query('SELECT * FROM `rooms` ORDER BY created');
        return $stmt->fetchAll();
    }

    function find($rooms_id) {
        $stmt = $this->db->prepare('SELECT * FROM `rooms` WHERE `id_rooms` = :id');
        $stmt->bindValue(':id', $rooms_id, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetch();
    }

    function getUsersInRoom($rooms_id) {
        $stmt = $this->db->prepare('SELECT * FROM in_room LEFT JOIN users USING(id_users) WHERE id_rooms = :ir ');
        $stmt->bindValue(':ir', $rooms_id);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    function delete($rooms_id) {
        $stmt = $this->db->prepare('DELETE FROM `rooms` WHERE `id_rooms` = :id');
        $stmt->bindValue(':id', $rooms_id, PDO::PARAM_INT);

        return $stmt->execute();
    }

    function lockOrUnlock($rooms_id) {
        $stmt = $this->db->prepare('SELECT `lock` FROM `rooms` WHERE `id_rooms` = :ir');
        $stmt->bindValue(':ir', $rooms_id, PDO::PARAM_INT);
        $stmt->execute();

        $fetchedRow = $stmt->fetch();
        var_dump($fetchedRow['lock']);
        if ($fetchedRow['lock'] == 'true') {
            $stmt = $this->db->prepare('UPDATE rooms SET `lock` = "false" WHERE `id_rooms` = :ir');
            $stmt->bindValue(':ir', $rooms_id, PDO::PARAM_INT);
            return $stmt->execute();
        } else {
            $stmt = $this->db->prepare('UPDATE rooms SET `lock` = "true" WHERE `id_rooms` = :ir');
            $stmt->bindValue(':ir', $rooms_id, PDO::PARAM_INT);
            return $stmt->execute();
        }
    }

    function kickUserFromRoom($rooms_id, $users_id) {
        $stmt = $this->db->prepare('INSERT INTO `room_kick` (id_users, id_rooms, created) VALUES (:iu, :ir, NOW())');
        $stmt->bindValue(':ir', $rooms_id, PDO::PARAM_INT);
        $stmt->bindValue(':iu', $users_id, PDO::PARAM_INT);

        $stmt->execute();
    }

    function isUserKicked($rooms_id, $users_id) {
        $stmt = $this->db->prepare('SELECT * FROM `room_kick` WHERE `id_rooms` = :ir AND id_users = :iu');
        $stmt->bindValue(':ir', $rooms_id, PDO::PARAM_INT);
        $stmt->bindValue(':iu', $users_id, PDO::PARAM_INT);

        $stmt->execute();
        $rows = $stmt->fetch();

        if ($rows) {
            return true;
        } else {
            return false;
        }
    }
}