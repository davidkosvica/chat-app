<?php


class Messages extends Model
{
    function getAllByRoomId($id) {
        $stmt = $this->db->prepare('SELECT messages.*, u1.login as login_from, u2.login as login_to FROM messages LEFT JOIN users u1 ON messages.id_users_from = u1.id_users LEFT JOIN  users u2 on messages.id_users_to = u2.id_users WHERE id_rooms = :ir ORDER BY created');
        $stmt->bindValue(':ir', $id);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    function add($roomId, $idUserFrom, $idUserTo = null, $message) {
        $stmt = $this->db->prepare('SELECT `lock` FROM `rooms` WHERE id_rooms = :ir');
        $stmt->bindValue(':ir', $roomId);
        $stmt->execute();
        $fetchedRoomRow = $stmt->fetch();
        if ($fetchedRoomRow['lock'] == 'false') {
            $stmt = $this->db->prepare('INSERT INTO messages 
            (id_rooms, id_users_from, id_users_to, created, message) 
            VALUES (:ir, :iuf, :iut, NOW(), :m)');

            $stmt->bindValue(':ir', $roomId);
            $stmt->bindValue(':iuf', $idUserFrom);
            $stmt->bindValue(':iut', $idUserTo);
            $stmt->bindValue(':m', $message);

            return $stmt->execute();
        } else {
            return false;
        }
    }

}