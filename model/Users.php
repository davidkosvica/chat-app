<?php

class Users extends Model
{
    function register(array $data) {
        $stmt = $this->db->prepare('INSERT INTO `users` 
        (`login`, `email`, `password`, `name`, `surname`, `gender`, `registered`) VALUES
        (:l, :e, :p, :n, :s, :g, NOW())');

        $p = password_hash($data['password'], PASSWORD_DEFAULT);

        $stmt->bindValue(':l', $data['login'], PDO::PARAM_STR);
        $stmt->bindValue(':e', $data['email'], PDO::PARAM_STR);
        $stmt->bindValue(':p', $p, PDO::PARAM_STR);
        $stmt->bindValue(':n', $data['name'], PDO::PARAM_STR);
        $stmt->bindValue(':s', $data['surname'], PDO::PARAM_STR);
        $stmt->bindValue(':g', $data['gender'], PDO::PARAM_STR);

        return $stmt->execute();
    }

    function getByLogin($login) {
        $stmt = $this->db->prepare('SELECT * FROM users WHERE login = :l');
        $stmt->bindValue(':l', $login);
        $stmt->execute();

        return $stmt->fetch();
    }

    function insertUserIntoRoom($userId, $roomId) {
        $stmt = $this->db->prepare('SELECT max(created) FROM messages WHERE id_users_from = :iuf AND id_rooms = :ir');
        $stmt ->bindValue(':iuf', $userId);
        $stmt ->bindValue(':ir', $roomId);
        $stmt->execute();
        $lastMessageTime = $stmt->fetch();

        $stmt = $this->db->prepare('INSERT INTO in_room (id_users, id_rooms, last_message, entered) VALUES (:iu, :ir, :lm, NOW()) ON DUPLICATE KEY UPDATE entered = NOW()');
        $stmt->bindValue(':iu', $userId);
        $stmt->bindValue(':ir', $roomId);
        $stmt->bindValue(':lm', $lastMessageTime ? $lastMessageTime['max(created)'] : null);

        return $stmt->execute();
    }

    function removeUserFromRoom($userId, $roomId) {
        $stmt = $this->db->prepare('DELETE FROM in_room WHERE id_users = :iu AND id_rooms = :ir');
        $stmt ->bindValue(':iu', $userId);
        $stmt ->bindValue(':ir', $roomId);

        return $stmt->execute();
    }

    function verify($login, $password) {
        $user = $this->getByLogin($login);
        if ($user) {
            if (password_verify($password, $user['password'])) {
                return $user;
            }
        }
        return null;
    }
}